<?php
/**
 * Реализовать проверку заполнения обязательных полей формы в предыдущей
 * с использованием Cookies, а также заполнение формы по умолчанию ранее
 * введенными значениями.
 */
header('Content-Type: text/html; charset=UTF-8');

$abilities = array(
  'immort' => "Бессмертие",
  'wall' => "Прохождение сквозь стены",
  'levit' => "Левитация",
  'invis' => "Невидимость");

// В суперглобальном массиве $_SERVER PHP сохраняет некторые заголовки запроса HTTP
// и другие сведения о клиненте и сервере, например метод текущего запроса $_SERVER['REQUEST_METHOD'].
if ($_SERVER['REQUEST_METHOD'] == 'GET') {
  // Массив для временного хранения сообщений пользователю.
  $messages = array();

  // В суперглобальном массиве $_COOKIE PHP хранит все имена и значения куки текущего запроса.
  // Выдаем сообщение об успешном сохранении.
  if (!empty($_COOKIE['save'])) {
    // Удаляем куку, указывая время устаревания в прошлом.
    setcookie('save', '', 100000);
    // Если есть параметр save, то выводим сообщение пользователю.
    $messages[] = 'Спасибо, результаты сохранены.';
  }


  // Складываем признак ошибок в массив.
  $errors = array();
  $errors['fio'] = !empty($_COOKIE['fio_error']);
  $errors['email'] = !empty($_COOKIE['email_error']);
  $errors['year'] = !empty($_COOKIE['year_error']);
  $errors['sex'] = !empty($_COOKIE['sex_error']);
  $errors['limbs'] = !empty($_COOKIE['limbs_error']);
  $errors['abilities'] = !empty($_COOKIE['abilities_error']);
  $errors['text'] = !empty($_COOKIE['text_error']);
  $errors['accept'] = !empty($_COOKIE['accept_error']);

  // Выдаем сообщения об ошибках.
  // fio
  if ($errors['fio']) {
    // Удаляем куку, указывая время устаревания в прошлом.
    setcookie('fio_error', '', 100000);
    // Выводим сообщение.
    if ($_COOKIE['fio_error'] == "1") {
      $messages[] = '<div class="error">Заполните имя.</div>';
    }
    else {
      $messages[] = '<div class="error">Укажите корректное имя.</div>';
    }
    
  }

    // email
    if ($errors['email']) {
      // Удаляем куку, указывая время устаревания в прошлом.
      setcookie('email_error', '', 100000);
      // Выводим сообщение.
      if ($_COOKIE['email_error'] == "1") {
        $messages[] = '<div class="error">Заполните email.</div>';
      }
      else {
        $messages[] = '<div class="error">Укажите корректный email.</div>';
    }

    // year
    if ($errors['year']) {
      // Удаляем куку, указывая время устаревания в прошлом.
      setcookie('year_error', '', 100000);
      // Выводим сообщение.
      if ($_COOKIE['year_error'] == "1") {
        $messages[] = '<div class="error">Заполните год.</div>';
      }
      else {
        $messages[] = '<div class="error">Укажите корректный год.</div>';
      }
    }
    
    // accept
    if ($errors['accept']) {
      // Удаляем куку, указывая время устаревания в прошлом.
      setcookie('accept_error', '', 100000);
      // Выводим сообщение.
      if ($_COOKIE['accept_error'] == "1") {
        $messages[] = '<div class="error">Вы не приняли соглашение.</div>';
      }  
    }

     // text
     if ($errors['text']) {
      // Удаляем куку, указывая время устаревания в прошлом.
      setcookie('text_error', '', 100000);
      $messages[] = '<div class="error">Заполните текстовое поле.</div>';
      // Выводим сообщение.
      // if ($_COOKIE['text_error'] == "1") {
       
      // }
    }

    // abilities
    if ($errors['abilities']) {
      // Удаляем куку, указывая время устаревания в прошлом.
      setcookie('abilities_error', '', 100000);
      // Выводим сообщение.
      if ($_COOKIE['abilities_error'] == "1") {
        $messages[] = '<div class="error">Выберите споособность</div>';
      }
      else {
        $messages[] = '<div class="error">Выбрана недопустимая способность</div>';
      }
      
    }

  }


  // Складываем предыдущие значения полей в массив, если есть.
  // Начальные значения всех полей формы, которые взяты из cookie
  $values = array();
  $values['fio'] = empty($_COOKIE['fio_value']) || !preg_match('/^[a-zA-Zа-яёА-ЯЁ\s\-]+$/u', $_COOKIE['fio_value']) ? '' : $_COOKIE['fio_value'];
  $values['email'] = empty($_COOKIE['email_value']) ? '' : $_COOKIE['email_value'];
  $values['year'] = empty($_COOKIE['year_value']) ? '' : $_COOKIE['year_value'];
  $values['sex'] = $_COOKIE['sex_value'] === '0' ? 0 : 1;
  $values['limbs'] = empty($_COOKIE['limbs_value']) ? '' : $_COOKIE['limbs_value'];
  if (!empty($_COOKIE['abilities_value'])) {
    $abilities_value = json_decode($_COOKIE['abilities_value']);
  }
  $values['abilities'] = array();//empty($_COOKIE['abilities_value']) ? '' : $_COOKIE['abilities_value'];
  if (is_array($abilities_value)) {
    foreach($abilities_value as $ability) {
      if (!empty($abilities[$ability])) {
        $values['abilities'][$ability] = $ability;
      }
    }
  }
  $values['text'] = empty($_COOKIE['text_value']) ? '' : $_COOKIE['text_value'];
  $values['accept'] = empty($_COOKIE['accept_value']) ? '' : $_COOKIE['accept_value'];

  // Включаем содержимое файла form.php.
  // В нем будут доступны переменные $messages, $errors и $values для вывода 
  // сообщений, полей с ранее заполненными данными и признаками ошибок.
  include('form.php');
}
// Иначе, если запрос был методом POST
else {
  // Проверяем ошибки.
  $errors = FALSE;
  // fio
  if (empty($_POST['fio'])) {
    setcookie('fio_error', '1', time() + 24 * 60 * 60);
    $errors = TRUE;
  }
  else {
    if (!preg_match('/^[a-zA-Zа-яёА-ЯЁ\s\-]+$/u', $_POST['fio'])) {
      setcookie('fio_error', '2', time() + 24 * 60 * 60);
      $errors = TRUE;
    }
    // Сохраняем ранее введенное в форму значение на месяц.
    setcookie('fio_value', $_POST['fio'], time() + 12 * 30 * 24 * 60 * 60);
  }

  // email
  if (empty($_POST['email'])) {
    // Выдаем куку на день с флажком об ошибке в поле email.
    setcookie('email_error', '1', time() + 24 * 60 * 60);
    $errors = TRUE;
  }
  else {
    if (!preg_match('/^[^@]+@[^@.]+\.[^@]+$/', $_POST['email'])) {
      setcookie('email_error', '2', time() + 24 * 60 * 60);
      $errors = TRUE;
    }
    // Сохраняем ранее введенное в форму значение на месяц.
    setcookie('email_value', $_POST['email'], time() + 12 * 30 * 24 * 60 * 60);
  }

  // year
  if (empty($_POST['year'])) {
    setcookie('year_error', '1', time() + 24 * 60 * 60);
    $errors = TRUE;
  }
  else {
    $year = $_POST['year'];
    if (!(is_numeric($year) && intval($year) >= 1900 && intval($year) <= 2020)) {
      setcookie('year_error', '2', time() + 24 * 60 * 60);  
      $errors = TRUE;
    }
    setcookie('year_value', $_POST['year'], time() + 12 * 30 * 24 * 60 * 60);
  }

  // sex
  setcookie('sex_value', $_POST['sex'], time() + 12 * 30 * 24 * 60 * 60);

  // limbs
  if (empty($_POST['limbs'])) {
    setcookie('limbs_error', '1', time() + 24 * 60 * 60);
    $errors = TRUE;
  }
  else {
    // Сохраняем ранее введенное в форму значение на месяц.
    setcookie('limbs_value', $_POST['limbs'], time() + 12 * 30 * 24 * 60 * 60);
  }

  //abilities
  if (empty($_POST['abilities'])) {
    setcookie('abilities_error', '1', time() + 24 * 60 * 60);
    $errors = TRUE;
  }
  else {
    $abilities_error = FALSE;
    foreach($_POST['abilities'] as $key) {
      if (empty($abilities[$key])) {
        setcookie('abilities_error', '2', time() + 24 * 60 * 60);
        $errors = TRUE;
        $abilities_error = TRUE;
      }
    }
    if (!$abilities_error) {
      setcookie('abilities_value', json_encode($_POST['abilities']), time() + 12 * 30 * 24 * 60 * 60);
    }
  }

  // abilities
  // $ability_data = array('immort', 'wall', 'levit', 'invis');
  // if (empty($_POST['abilities'])) {
  //     print('Выберите способность<br>');
  //     $errors = TRUE;
  // }
  // else {
  //     $abilities = $_POST['abilities'];
  //     foreach ($abilities as $ability) {
  //         if (!in_array($ability, $ability_data)) {
  //             print('Недопустимая способность<br>');
  //             $errors = TRUE;
  //         }
  //     }
  // }
  // $ability_insert = array();
  // foreach ($ability_data as $ability) {
  //     $ability_insert[$ability] = in_array($ability, $abilities) ? 1 : 0;
  // }

  // text
  if (empty($_POST['text'])) {
    setcookie('text_error', '1', time() + 24 * 60 * 60);
    $errors = TRUE;
  }
  else {
    setcookie('text_value', $_POST['text'], time() + 12 * 30 * 24 * 60 * 60);
  }


  // accept
  if (!isset($_POST['accept'])) {
    setcookie('accept_error', '1', time() + 24 * 60 * 60);
    $errors = TRUE;
  }
  else {
    setcookie('accept_value', $_POST['accept'], time() + 12 * 30 * 24 * 60 * 60);
  }
  

// *************
// TODO: тут необходимо проверить правильность заполнения всех остальных полей.
// Сохранить в Cookie признаки ошибок и значения полей.
// *************

  if ($errors) {
    // При наличии ошибок перезагружаем страницу и завершаем работу скрипта.
    header('Location: index.php');
    exit();
  }
  else {
    // Удаляем Cookies с признаками ошибок.
    setcookie('fio_error', '', 100000);
    setcookie('email_error', '', 100000);
    setcookie('year_error', '', 100000);
    setcookie('sex_error', '', 100000);
    setcookie('limbs_error', '', 100000);
    setcookie('abilities_error', '', 100000);
    setcookie('text_error', '', 100000);
    setcookie('accept_error', '', 100000);
  }

  
  // Сохранение в БД
  // ...
  // Сохранение в базу данных.

$user = 'u20384';
$pass = '2246484';
$db = new PDO('mysql:host=localhost;dbname=u20384', $user, $pass, array(PDO::ATTR_PERSISTENT => true));

// Подготовленный запрос. Не именованные метки.
try {
  $stmt = $db->prepare("INSERT INTO application SET name = ?, email = ?, year = ?, sex = ?, limbs = ?, abilities =?, text = ?, accept = ?");
  $stmt -> execute(array($_POST['fio'], $_POST['email'], intval($_POST['year']), intval($_POST['sex']), intval($_POST['limbs']), json_encode($abilities), $_POST['text'], intval($_POST['accept'])));
}
// $ability_insert['immort'], $ability_insert['wall'], $ability_insert['levit'], $ability_insert['invis']
catch(PDOException $e) {
  print('Error : ' . $e->getMessage());
  exit();
}

  // Сохраняем куку с признаком успешного сохранения.
  setcookie('save', '1');

  // Делаем перенаправление.
  header('Location: index.php');
} 